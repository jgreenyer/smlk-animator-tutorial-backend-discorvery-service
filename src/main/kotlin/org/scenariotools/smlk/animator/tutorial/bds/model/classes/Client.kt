package org.scenariotools.smlk.animator.tutorial.bds.model.classes

import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c

open class Client(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")), IClient {

}

interface IClient{
    fun returnBackend(backend: IBackend) = event(backend){}
}
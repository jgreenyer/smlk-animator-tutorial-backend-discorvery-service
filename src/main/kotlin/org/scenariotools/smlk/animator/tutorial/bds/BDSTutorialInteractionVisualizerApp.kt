package org.scenariotools.smlk.animator.tutorial.bds

import javafx.stage.Stage
import org.scenariotools.smlk.animator.tutorial.bds.cases.configurations
import org.scenriotools.smlk.animator.style.InteractionAnimatorStyles
import org.scenriotools.smlk.animator.view.InteractionAnimatorView
import tornadofx.App
import tornadofx.find
import tornadofx.launch


fun main(args: Array<String>) {
    launch<InteractionVisualizerApp>(*args)
}


class InteractionVisualizerApp: App(InteractionAnimatorView::class, InteractionAnimatorStyles::class){

    override fun start(stage: Stage) {
        super.start(stage)

        val view = find(primaryView, scope)

        val interactionAnimatorView = view as InteractionAnimatorView
        val interactionSystem = interactionAnimatorView.isvm.item

        interactionSystem.updateInteractionSystemConfigurations(configurations)

    }

}

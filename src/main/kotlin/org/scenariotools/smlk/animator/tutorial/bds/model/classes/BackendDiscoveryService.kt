package org.scenariotools.smlk.animator.tutorial.bds.model.classes

import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c

open class BackendDiscoveryService(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")), IBackendDiscoveryService {

}

interface IBackendDiscoveryService {

}

package org.scenariotools.smlk.animator.tutorial.bds.cases.simpleconfiguration

import org.scenariotools.smlk.animator.tutorial.bds.model.classes.Backend
import org.scenariotools.smlk.animator.tutorial.bds.model.classes.BackendDiscoveryService
import org.scenariotools.smlk.animator.tutorial.bds.model.classes.Client
import org.scenariotools.smlk.animator.tutorial.bds.model.classes.Device
import org.scenariotools.smlk.symbolicEvent
import org.scenriotools.smlk.animator.model.Instance
import org.scenriotools.smlk.animator.model.InteractionSystemConfiguration
import org.scenriotools.smlk.animator.smlkextension.shadowScenario

object Device1 : Device("Device 1", 100.0, 300.0){}
object Device2 : Device("Device 2", 100.0, 300.0){}
object Device3 : Device("Device 3", 100.0, 300.0){}

object BackendA : Backend("Backend A", 100.0, 500.0){}
object BackendB : Backend("Backend B", 100.0, 500.0){}

object BDS : BackendDiscoveryService("BDS", 400.0, 500.0){}

object Client1 : Client("Client", 500.0, 100.0){}

val instances = listOf<Instance>(
    Device1,
    Device2,
    Device3,
    BackendA,
    BackendB,
    BDS,
    Client1
)


//fun configurationFourServers() : Pair<String, InteractionSystemConfiguration>{
//
//    val configuration = InteractionSystemConfiguration(
//        1900.0, 900.0,
//        environmentMessageTypes = environmentMessageTypes,
//        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
//        instances = instances,
//        testScenario = ???,
//        guaranteeScenarios = setOf(scenarios_v1, setOf(shadowScenario)).flatten().toSet()
//    )
//
//    return "Simple Test Interaction System -- FourServers" to configuration
//}
